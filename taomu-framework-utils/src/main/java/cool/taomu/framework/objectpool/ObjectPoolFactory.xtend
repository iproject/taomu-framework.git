/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.objectpool

import cool.taomu.framework.configure.ConfigureManage
import cool.taomu.framework.utils.reflect.ReflectUtils
import java.lang.annotation.Annotation
import java.lang.invoke.MethodHandles
import java.lang.invoke.MethodType
import java.util.concurrent.atomic.AtomicInteger
import org.apache.commons.pool2.BaseKeyedPooledObjectFactory
import org.apache.commons.pool2.PooledObject
import org.apache.commons.pool2.impl.DefaultPooledObject
import org.apache.commons.pool2.impl.GenericKeyedObjectPool
import org.apache.commons.pool2.impl.GenericKeyedObjectPoolConfig
import org.slf4j.LoggerFactory

class ObjectPoolFactory extends BaseKeyedPooledObjectFactory<Class<Object>, ObjectPool> {

	var idCount = new AtomicInteger(1);
	val static LOG = LoggerFactory.getLogger(ObjectPoolFactory)
	static Object pool = null;
	val static SPRING_AUTOWIRED = "org.springframework.beans.factory.annotation.Autowired";

	def static synchronized GenericKeyedObjectPool<Class<Object>, ObjectPool> getInstance() {
		if (pool === null) {
			var config = new GenericKeyedObjectPoolConfig<ObjectPool>();
			var yml = ConfigureManage.loadConfig;
			config.setMaxTotalPerKey(yml.pool.maxTotalPerKey);
			config.setMaxIdlePerKey(yml.pool.maxIdlePerKey);
			config.setMinIdlePerKey(yml.pool.minIdlePerKey);
			config.setLifo(yml.pool.lifo);
			pool = new GenericKeyedObjectPool<Class<Object>, ObjectPool>(new ObjectPoolFactory(), config);
		}
		return pool as GenericKeyedObjectPool<Class<Object>, ObjectPool>
	}

	def static perparePool(Class<Object> key) {
		ObjectPoolFactory.instance.preparePool(key)
	}

	def static borrowObject(Class<Object> key) {
		ObjectPoolFactory.instance.borrowObject(key)
	}

	def static returnObject(Class<Object> key, ObjectPool obj) {
		ObjectPoolFactory.instance.returnObject(key, obj)
	}

	override PooledObject<ObjectPool> makeObject(Class<Object> key) throws Exception {
		LOG.info("创建对象:" + key)
		return super.makeObject(key);
	}

	override activateObject(Class<Object> key, PooledObject<ObjectPool> p) throws Exception {
		LOG.info("活动对象：" + key.name + " ==> " + p.object.id)
		super.activateObject(key, p);
	}

	override destroyObject(Class<Object> key, PooledObject<ObjectPool> p) throws Exception {
		LOG.info("销毁对象:" + key + " ==> " + p.object.id)
		super.destroyObject(key, p);
	}

	override passivateObject(Class<Object> key, PooledObject<ObjectPool> p) throws Exception {
		LOG.info("归还对象:" + key + " ==> " + p.object.id)
		super.passivateObject(key, p);
	}

	override wrap(ObjectPool value) {
		return new DefaultPooledObject<ObjectPool>(value);
	}

	override create(Class<Object> zlass) throws Exception {
		var instance = zlass.newInstance;
		var fields = ReflectUtils.getReflectFields(zlass);
		for (field : fields) {
			var ClassLoader loader = Thread.currentThread().getContextClassLoader();
			try {
				var ann = field.getAnnotation(loader.loadClass(SPRING_AUTOWIRED) as Class<Annotation>);
				if (ann !== null) {
					ReflectUtils.setReflectFields(zlass, field.name, instance, this.getSpringBean(field.type))
				}
			} catch (ClassNotFoundException ex) {
				LOG.debug("Class Not Found Exception", ex)
			}
		}
		return new ObjectPool(instance, idCount.getAndAdd(1));
	}

	def Object getSpringBean(Class<?> zlass) {
		try {
			var ClassLoader loader = Thread.currentThread().getContextClassLoader();
			var Class<?> registerBeanToSpring = loader.loadClass("cool.taomu.framework.springplugin.TaomuFramework");
			if (registerBeanToSpring !== null) {
				var desc = MethodType.methodType(Object, Class);
				var methodHandle = MethodHandles.lookup().findStatic(registerBeanToSpring, "getBean", desc);
				var bean = methodHandle.invokeExact(zlass);
				bean !== null ? LOG.info("从spring中成功获取一个bean") : LOG.info("从spring中未获取到一个bean");
				return bean;
			}
		} catch (Throwable e) {
			LOG.debug("", e)
		}
		return null;
	}

}
