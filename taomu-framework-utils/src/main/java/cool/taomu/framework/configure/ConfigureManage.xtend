/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.configure

import cool.taomu.framework.configure.entity.ConfigureEntity
import cool.taomu.framework.configure.entity.MqttEntity
import cool.taomu.framework.configure.entity.ObjectPoolEntity
import cool.taomu.framework.configure.entity.RpcEntity
import java.io.FileReader
import java.net.URL
import org.yaml.snakeyaml.Yaml

class ConfigureManage {
	public static String configPath = "taomu.yml";

	def static loadConfig() {
		var url = Thread.currentThread().getContextClassLoader().getResource(configPath);
		if (url === null) {
			url = new URL("file://" + configPath);
		}
		var config = new ConfigureEntity();
		if (url !== null) {
			config = new Yaml().loadAs(new FileReader(url.getPath()), ConfigureEntity);
			if (config.mqtt === null) {
				config.mqtt = new MqttEntity();
			}
			if (config.rpc === null) {
				config.rpc = new RpcEntity();
			}
			if (config.pool === null) {
				config.pool = new ObjectPoolEntity();
			}
		}
		return config;
	}
}
