package cool.taomu.framework.configure.entity

import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

@Accessors
@ToString
class JeidsConfigEntity {
    List<HostEntity> cluster;
    String username;
    String password;
    Integer maxIdle;
    Integer minIdle;
    Integer maxTotal;
    Integer timeout = 5000;
    Integer maxAttempts = 5;
    boolean testOnBorrow = true;
    boolean testOnReturn = false;
    boolean isClustered = false;
}
