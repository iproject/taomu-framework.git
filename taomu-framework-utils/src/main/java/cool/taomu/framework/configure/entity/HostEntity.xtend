package cool.taomu.framework.configure.entity

import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

@Accessors
@ToString
class HostEntity {
    String ip = "0.0.0.0";
    Integer port;

    new() {
    }

    new(String ip, Integer port) {
        this.ip = ip;
        this.port = port;
    }
}
