package cool.taomu.framework.configure.entity

import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

@Accessors
@ToString
class ClusterEntity {
	String hostname = "0.0.0.0";
	Integer port = 32768;
}
