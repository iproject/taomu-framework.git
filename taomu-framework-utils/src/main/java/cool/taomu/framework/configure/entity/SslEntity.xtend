package cool.taomu.framework.configure.entity

import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

@Accessors
@ToString
class SslEntity {
    boolean useClientCA;
    String keyStoreType;
    String keyFilePath;
    String managerPwd;
    String storePwd;
}
