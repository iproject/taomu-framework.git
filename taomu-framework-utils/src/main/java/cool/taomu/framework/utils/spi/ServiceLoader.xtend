/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.utils.spi

import com.google.common.collect.HashBasedTable
import com.google.common.collect.Table
import java.io.File
import java.net.URL
import java.util.Enumeration
import java.util.Objects
import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.ObjectUtils
import org.slf4j.LoggerFactory

import static extension cool.taomu.framework.utils.reflect.ReflectUtils.*

final class ServiceLoader<S> {
	static val LOG = LoggerFactory.getLogger(ServiceLoader);
	static val PREFIX = "META-INF/services/";
	static val SCRIPT_PREFIX = "script/";
	val Class<S> service;
	val String name;
	val ClassLoader loader;
	var Object[] args;
	val static Table<String, String, Class<?>> providers = HashBasedTable.create();
	val static Table<String, String, String> scriptProviders = HashBasedTable.create();

	private def loadZlass() {
		val String fullName = PREFIX + service.name;
		var Enumeration<URL> configs;
		if (loader === null) {
			configs = ClassLoader.getSystemResources(fullName);
		} else {
			configs = loader.getResources(fullName);
		}
		loadZlass(configs)
	}

	protected def void loadZlass(Enumeration<URL> configs) {
		while (configs.hasMoreElements) {
			var String path = configs.nextElement.toString;
			LOG.info(path);
			var strings = FileUtils.readFileToString(new File(path.replace("file:", "")), "UTF-8");
			strings.split("\n").filterNull.forEach [
				var Class<?> zlass;
				try {
					zlass = Class.forName(it, false, loader);
					val alias = zlass.getAnnotation(Alias);
					if (alias !== null && !alias.value.equals("")) {
						providers.put(name, alias.value, zlass);
					} else {
						providers.put(name, zlass.simpleName, zlass);
					}
				} catch (ClassNotFoundException ex) {
					LOG.info("类 " + it + " 不存在", ex)
				}
				if (!service.isAssignableFrom(zlass)) {
					LOG.info("类" + it + " 不是 " + name + " subtype");
				}
			]
		}
	}

	private new(Class<S> svc, ClassLoader cl, boolean isReload, Object... args) {
		service = Objects.requireNonNull(svc, "class 不能为 null");
		this.name = svc.name;
		loader = ObjectUtils.defaultIfNull(cl, ClassLoader.getSystemClassLoader());
		if (isReload) {
			providers.row(this.name).clear;
		}
		loadZlass();
		this.args = args;
	}

	def S get(String key) {
		var obj = this.getZlass(key).newInstance(this.args);
		return service.cast(obj);
	}

	def S get(Integer index) {
		var pro = providers.row(name).values;
		if (pro.size == 0) {
			return null;
		}
		var obj = providers.row(name).values.get(index).newInstance(this.args);
		return service.cast(obj);
	}

	def get() {
		var keySet = providers.row(name).keySet
		return keySet.map[service.cast(providers.get(name, it).newInstance(this.args))]
	}

	def Class<?> getZlass(String key) {
		return providers.get(name, key);
	}

	def S first() {
		return this.get(0);
	}

	def static <S> ServiceLoader<S> load(Class<S> service, ClassLoader loader, Object ... args) {
		return new ServiceLoader<S>(service, loader, false, args);
	}

	def static <S> ServiceLoader<S> load(Class<S> service, Object... args) {
		return ServiceLoader.load(service, Thread.currentThread().getContextClassLoader(), args);
	}

	def static <S> ServiceLoader<S> load(Class<S> service) {
		return ServiceLoader.load(service, Thread.currentThread().getContextClassLoader(), null);
	}

	def static <S> ServiceLoader<S> reload(Class<S> service, ClassLoader loader, Object ... args) {
		return new ServiceLoader<S>(service, loader, true, args);
	}

	def static <S> ServiceLoader<S> reload(Class<S> service, Object... args) {
		return ServiceLoader.reload(service, Thread.currentThread().getContextClassLoader(), args);
	}

}
