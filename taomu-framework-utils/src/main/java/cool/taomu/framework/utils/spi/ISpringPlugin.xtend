package cool.taomu.framework.utils.spi

interface ISpringPlugin {
	def Object getBeanObject(Class<?> zlass);
}
