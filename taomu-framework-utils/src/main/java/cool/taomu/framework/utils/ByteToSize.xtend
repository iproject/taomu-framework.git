package cool.taomu.framework.utils

import java.text.DecimalFormat
import java.math.BigDecimal

class ByteToSize {
	def static void main(String[] args) {
		println(sizeDescription("11258999068426"));
	}

	def static sizeDescription(String size) {
		var bsize = new BigDecimal(size);
		var format = new DecimalFormat("###.0");
		if (bsize.compareTo(new BigDecimal("1125899906842624")) > -1) {
			var a = new BigDecimal("1125899906842624");
			var result = bsize.divide(a).longValue;
			return #[format.format(result), "PB"].join()
		} else if (bsize.compareTo(new BigDecimal("1099511627776")) > -1) {
			var a = new BigDecimal("1099511627776");
			var result = bsize.divide(a).longValue;
			return #[format.format(result), "TB"].join()
		} else if (bsize.longValue >= 1024 * 1024 * 1024) {
			var result = (bsize.longValue / (1024.0 * 1024.0 * 1024.0));
			return #[format.format(result), "GB"].join()
		} else if (bsize.longValue >= 1024 * 1024) {
			var result = (bsize.longValue / (1024.0 * 1024.0));
			return #[format.format(result), "MB"].join()
		} else if (bsize.longValue >= 1024) {
			var result = (bsize.longValue / 1024.0);
			return #[format.format(result), "KB"].join()
		} else if (bsize.longValue < 1024) {
			if (bsize.longValue <= 0) {
				return "0B";
			} else {
				return #[size, "B"].join();
			}
		}
	}

}
