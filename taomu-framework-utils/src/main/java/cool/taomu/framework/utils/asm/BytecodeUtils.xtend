/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.utils.asm

import cool.taomu.framework.configure.Constant
import java.io.PrintWriter
import java.lang.invoke.CallSite
import java.lang.invoke.MethodHandles
import java.lang.invoke.MethodType
import java.lang.reflect.Constructor
import java.util.UUID
import java.util.concurrent.ConcurrentHashMap
import org.eclipse.xtend.lib.annotations.Accessors
import org.objectweb.asm.ClassVisitor
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Handle
import org.objectweb.asm.Label
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Type
import org.objectweb.asm.commons.GeneratorAdapter
import org.objectweb.asm.commons.Method
import org.objectweb.asm.util.TraceClassVisitor

final class BytecodeUtils extends ClassLoader {
	@Accessors
	static final class Variable {
		Integer index;
		Type type;

		new(Integer index, Type type) {
			this.index = index;
			this.type = type;
		}
	}

	ClassWriter cwriter;
	ClassVisitor tcvisitor;
	GeneratorAdapter ga;
	val String name;
	String superClassName;
	int version;
	val localVariable = new ConcurrentHashMap<String, Variable>();
	val localLabel = new ConcurrentHashMap<String, Label>();

	/**
	 * 定义一个接口用来提供除构造函数外的字节码操作
	 */
	static interface Asm<T> {
		def void coding(T code);
	}

	new() {
		this(
			String.format("%s.%s%s", Constant.DEFAULT_PACKAGE, Constant.DEFAULT_ClASS_PREFIX,
				UUID.randomUUID.toString.replace("-", "")).replace(".", "/"));
	}

	new(String name) {
		this(Opcodes.V1_8, name);
	}

	new(int version, String name) {
		cwriter = new ClassWriter(ClassWriter.COMPUTE_MAXS + ClassWriter.COMPUTE_FRAMES);
		tcvisitor = new TraceClassVisitor(cwriter, new PrintWriter(System.err))
		this.name = name;
		this.version = version;
	}

	def createClass(int access, Class<?> superClass, String ... inters) {
		superClassName = superClass.typeName
		tcvisitor.visit(version, access, name, null, superClassName, inters)
		return this;
	}

	def createClass(Class<?> superClass, String ... inters) {
		return this.createClass(Opcodes.ACC_PUBLIC, superClass, inters);
	}

	def createClass(String ... inters) {
		return this.createClass(Object, inters);
	}

	def createClass() {
		return this.createClass(null);
	}

	def createInterface(String ... inters) {
		return this.createClass(Opcodes.ACC_PUBLIC + Opcodes.ACC_ABSTRACT + Opcodes.ACC_INTERFACE, Object, inters);
	}

	def createAbstract(Class<?> superClass, String ... inters) {
		return this.createClass(Opcodes.ACC_PUBLIC + Opcodes.ACC_ABSTRACT, superClass, inters);
	}

	def createAbstract(String ... inters) {
		return this.createAbstract(Object, inters);
	}

	def createAbstract() {
		return this.createAbstract(null);
	}

	def init(Constructor<?>[] constructor) {
		constructor.filterNull.forEach [
			var m = Method.getMethod(it);
			var ga = new GeneratorAdapter(Opcodes.ACC_PUBLIC, m, null, null, tcvisitor);
			ga.loadThis();
			if (it.parameters.size > 0) {
				ga.loadArgs(0, it.parameters.size - 1);
			}
			ga.invokeConstructor(Type.getObjectType(superClassName), m);
			ga.returnValue();
			ga.endMethod();
		]
		return this;
	}

	def createField(String name, int access, Class<?> type, Object value) {
		tcvisitor.visitField(access, name, Type.getType(type).getDescriptor(), null, value).visitEnd();
		if (access == Opcodes.ACC_PRIVATE || access == Opcodes.ACC_PROTECTED) {
			var fName = StringExtensions.toFirstUpper(name)
			var m = this.createMethod(#["void", #["set", fName].join(), "(", type.name, ")"].join(" "), false);
			m.coding(GeneratorAdapter, new Asm<GeneratorAdapter>() {
				override coding(GeneratorAdapter it) {
					it.loadThis;
					it.loadArg(0);
					it.putField(Type.getObjectType(BytecodeUtils.this.name), name, Type.getType(type));
				}

			}).returnValue.endMethod;
			m = this.createMethod(#[type.name, #["get", fName].join(), "(", ")"].join(" "), false);
			m.coding(GeneratorAdapter, new Asm<GeneratorAdapter>() {
				override coding(GeneratorAdapter it) {
					it.loadThis;
					it.getField(Type.getObjectType(BytecodeUtils.this.name), name, Type.getType(type));
				}
			}).returnValue.endMethod;
		}
		return this
	}

	def createMethod(String method, boolean defaultPackage) {
		return this.createMethod(Opcodes.ACC_PUBLIC, method, defaultPackage);
	}

	def createMethod(String method) {
		return this.createMethod(method, true);
	}

	def createMethod(int access, String method, boolean defaultPackage) {
		ga = new GeneratorAdapter(access, Method.getMethod(method, defaultPackage), null, null, tcvisitor);
		return this;
	}

	def createMethod(java.lang.reflect.Method method) {
		this.createMethod(Opcodes.ACC_PUBLIC, method);
	}

	def createMethod(int access, java.lang.reflect.Method method) {
		ga = new GeneratorAdapter(access, Method.getMethod(method), null, null, tcvisitor);
		return this;
	}

	def invokeVirtual(String internalName, String method, boolean defaultPackage) {
		ga.invokeVirtual(Type.getObjectType(internalName.replace(".", "/")), Method.getMethod(method, defaultPackage));
		return this;
	}

	def invokeVirtual(String internalName, String method) {
		return this.invokeVirtual(internalName, method, true);
	}

	def invokeVirtual(Class<?> zlass, java.lang.reflect.Method method) {
		return this.invokeVirtual(zlass.name, method);
	}

	def invokeVirtual(Class<?> zlass, String method) {
		return this.invokeVirtual(zlass.name, method);
	}

	def invokeVirtual(String internalName, java.lang.reflect.Method method) {
		ga.invokeVirtual(Type.getObjectType(internalName.replace(".", "/")), Method.getMethod(method));
		return this;
	}

	def invokeStatic(Class<?> type, String method) {
		return this.invokeStatic(type.name, method, true);
	}

	def invokeStatic(String internalName, String method, boolean defaultPackage) {
		ga.invokeStatic(Type.getObjectType(internalName.replace(".", "/")), Method.getMethod(method, defaultPackage));
		return this;
	}

	def invokeDynamic(int tag, String internalName, Method method) {
		var mt = MethodType.methodType(CallSite, MethodHandles.Lookup, String, MethodType);
		var owner = Type.getObjectType(internalName.replace(".", "/")).internalName;
		var bootstrap = new Handle(tag, owner, "bootstrap", mt.toMethodDescriptorString(), false);
		ga.invokeDynamic(method.name, method.descriptor, bootstrap);
		return this;
	}

	def invokeDynamic(Class<?> zlass, Method method) {
		return this.invokeDynamic(Opcodes.H_INVOKESTATIC, zlass.name, method);
	}

	def returnValue() {
		ga.returnValue();
		return this;
	}

	def load(String key) {
		if (this.localVariable.containsKey(key)) {
			val local = this.localVariable.get(key);
			ga.loadLocal(local.index, local.type);
		}
		return this;
	}

	def store(String key, Class<?> zlass) {
		var index = ga.newLocal(Type.getType(zlass));
		ga.storeLocal(index, Type.getType(zlass));
		this.localVariable.put(key, new Variable(index, Type.getType(zlass)));
		return this;
	}

	def mark(String key) {
		if (this.localLabel.containsKey(key)) {
			ga.mark(this.localLabel.get(key));
		} else {
			var label = ga.newLabel;
			this.localLabel.put(key, label);
			ga.mark(label);
		}
		return this;
	}

	def goTo(String key) {
		if (this.localLabel.containsKey(key)) {
			ga.goTo(this.localLabel.get(key));
		} else {
			var label = ga.newLabel;
			this.localLabel.put(key, label);
			ga.goTo(label);
		}
		return this;
	}

	def ifNull(String key) {
		if (this.localLabel.containsKey(key)) {
			ga.ifNull(this.localLabel.get(key));
		} else {
			var label = ga.newLabel;
			this.localLabel.put(key, label);
			ga.ifNull(this.localLabel.get(key));
		}
		return this;
	}

	def ifNonNull(String key) {
		if (this.localLabel.containsKey(key)) {
			ga.ifNonNull(this.localLabel.get(key));
		} else {
			var label = ga.newLabel;
			this.localLabel.put(key, label);
			ga.ifNonNull(this.localLabel.get(key));
		}
		return this;
	}

	def ifCmp(String key, int mode, Class<?> zlass) {
		if (this.localLabel.containsKey(key)) {
			ga.ifCmp(Type.getType(zlass), mode, this.localLabel.get(key));
		} else {
			var label = ga.newLabel;
			this.localLabel.put(key, label);
			ga.ifCmp(Type.getType(zlass), mode, label);
		}
		return this;
	}

	def checkCast(Class<?> type) {
		ga.checkCast(Type.getType(type));
		return this;
	}

	def dup() {
		ga.dup();
		return this;
	}

	def push(int index) {
		ga.push(index);
		return this;
	}

	def push(String value) {
		ga.push(value);
		return this;
	}

	def loadArg(int index) {
		ga.loadArg(index);
		return this;
	}

	def <T> coding(Class<T> zlass, Asm<T> code) {
		if (zlass.equals(BytecodeUtils)) {
			code.coding(zlass.cast(this));
		} else if (zlass.equals(GeneratorAdapter)) {
			code.coding(ga as T);
		}
		return this;
	}

	def endMethod() {
		ga.endMethod();
		return this;
	}

	def buildReturnValue(java.lang.reflect.Method m) {
		switch (m.returnType) {
			case int: {
				this.checkCast(Integer);
				this.invokeVirtual(Integer, "int intValue()");
			}
			case long: {
				this.checkCast(Long);
				this.invokeVirtual(Long, "long longValue()");
			}
			case double: {
				this.checkCast(Double);
				this.invokeVirtual(Double, "double doubleValue()");
			}
			case float: {
				this.checkCast(Float);
				this.invokeVirtual(Float, "float floatValue()");
			}
			case boolean: {
				this.checkCast(Boolean);
				this.invokeVirtual(Boolean, "boolean booleanValue()");
			}
			case short: {
				this.checkCast(Short);
				this.invokeVirtual(Short, "short shortValue()");
			}
			case char: {
				this.checkCast(Character);
				this.invokeVirtual(Character, "char charValue()");
			}
			case byte: {
				this.checkCast(Byte);
				this.invokeVirtual(Byte, "byte byteValue()");
			}
			case void: {
			}
			default: {
				this.checkCast(m.returnType);
			}
		}
		return this;
	}

	def buildArguments(Method method) {
		if (method.argumentTypes.size > 0) {
			for (var index = 0; index < method.argumentTypes.size; index++) {
				this.dup();
				this.push(index);
				switch (method.argumentTypes.get(index).descriptor) {
					case "Z": {
						this.loadArg(index);
						this.invokeStatic(Boolean, "Boolean valueOf(boolean)");
					}
					case "C": {
						this.loadArg(index);
						this.invokeStatic(Character, "Character valueOf(char)");
					}
					case "B": {
						this.loadArg(index);
						this.invokeStatic(Byte, "Byte valueOf(byte)");
					}
					case "S": {
						this.loadArg(index);
						this.invokeStatic(Short, "Short valueOf(short)");
					}
					case "I": {
						this.loadArg(index);
						this.invokeStatic(Integer, "Integer valueOf(int)");
					}
					case "F": {
						this.loadArg(index);
						this.invokeStatic(Float, "Float valueOf(float)");
					}
					case "J": {
						this.loadArg(index);
						this.invokeStatic(Float, "Long valueOf(long)");
					}
					case "D": {
						this.loadArg(index);
						this.invokeStatic(Float, "Double valueOf(double)");
					}
					default: {
						ga.visitVarInsn(Opcodes.ALOAD, index + 1);
					}
				}
				ga.visitInsn(Opcodes.AASTORE);
			}
		} else {
			ga.visitInsn(Opcodes.ACONST_NULL);
		}
		return this;
	}

	def end() {
		tcvisitor.visitEnd();
		return this;
	}

	def toByteArray() {
		return cwriter.toByteArray();
	}

	override findClass(String name) {
		var bytes = toByteArray();
		return super.defineClass(name, bytes, 0, bytes.length);
	}
}
