package cool.taomu.framework.crypto.ssl

import java.io.File
import java.io.FileInputStream
import java.math.BigInteger
import java.security.KeyStore
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

class LocalX509TrustManager implements X509TrustManager {
	String filePath;

	override checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		if (chain === null) {
			throw new CertificateException("");
		}
		if (chain.length < 1) {
			throw new CertificateException("");
		}
		if (!(authType !== null && authType.equals("ECDHE_RSA"))) {
			throw new CertificateException("");
		}
		var factory = TrustManagerFactory.getInstance("X509");
		factory.init(null as KeyStore);
		factory.trustManagers.forEach[(it as X509TrustManager).checkServerTrusted(chain, authType)]
		// TODO 这个地方需要修改成pem的
		var certificateFactory = CertificateFactory.getInstance("X.509")
		var input = new FileInputStream(new File(""));//getAssets().open("baidu.cer");
		var X509Certificate x509certificate = certificateFactory.generateCertificate(input) as X509Certificate;
		var clientEncoded = new BigInteger(1, x509certificate.publicKey.encoded).toString(16);
		var clientSubject = x509certificate.subjectDN.name;
		var clientIssUser = x509certificate.issuerDN.name;
		var certificate = chain.get(0);
		var publicKey = certificate.publicKey
		var serverEncoded = new BigInteger(1, publicKey.encoded).toString(16);
		if (!clientEncoded.equals(serverEncoded)) {
			throw new CertificateException("");
		}
		if (!clientSubject.equals(certificate.subjectDN.name)) {
			throw new CertificateException("");
		}
		if (!clientIssUser.equals(certificate.issuerDN.name)) {
			throw new CertificateException("");
		}
	}

	override getAcceptedIssuers() {
		return #[].get(0)
	}

}




















