/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.crypto;

public class Base64 implements ICrypto {
    private ICrypto c;
    private byte[] src;

    public Base64(byte[] src) {
    	this.src = src;
    }

    public Base64(ICrypto c) {
        this.c = c;
    }

    @Override
    public byte[] encode() {
        if (c != null) {
            return org.apache.commons.codec.binary.Base64.encodeBase64(c.encode());
        }
        return org.apache.commons.codec.binary.Base64.encodeBase64(this.src);
    }

    @Override
    public byte[] decode() {
        if (c != null) {
        	byte[] base64Src = org.apache.commons.codec.binary.Base64.decodeBase64(this.c.getData());
        	this.c.setData(base64Src);
            return c.decode();
        }else {
        	return  org.apache.commons.codec.binary.Base64.decodeBase64(this.src);
        }
    }

	@Override
	public byte[] getData() {
		return this.src;
	}

	@Override
	public ICrypto setData(byte[] src) {
		this.src = src;
		return this;
	}

}
