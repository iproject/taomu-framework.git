package cool.taomu.demo

import cool.taomu.framework.client.mqtt.MqttCallback
import cool.taomu.framework.client.mqtt.annotation.Topic
import cool.taomu.framework.crypto.Base64
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

interface ITSpring {
	def byte[] getBase64(byte[] a);
}

@Service
class TSpring implements ITSpring {
	final static Logger LOG = LoggerFactory.getLogger(TSpring);

	override getBase64(byte[] a) {
		LOG.info("UTF-8");
		var b64 = new Base64(a);
		return b64.decode;
	}
}

@Topic(value=#["topic_a1","topic_a2"])
class MqttDemo extends MqttCallback {

	final static Logger LOG = LoggerFactory.getLogger(MqttDemo);

	@Autowired
	ITSpring tSpring;

	override messageArrived(String topic, MqttMessage message) throws Exception {
		// var b64 = new Base64(message.payload);
		LOG.info(new String(this.tSpring.getBase64(message.payload), "UTF-8"));
	}
}
