package cool.taomu.demo

import cool.taomu.framework.client.mqtt.Subscriber
import cool.taomu.framework.service.rpc.Gateway
import cool.taomu.framework.service.rpc.TRpcClient
import cool.taomu.framework.utils.spi.ISpringPlugin
import cool.taomu.framework.utils.spi.ServiceLoader
import org.apache.thrift.protocol.TProtocol
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan(basePackages=#["cool.taomu.demo"])
class CreateMqttClient {

	def static void main(String[] args) {
		var app = SpringApplication.run(CreateMqttClient, args);
		var isp = ServiceLoader.load(ISpringPlugin, app);
		var sub = new Subscriber("0.0.0.0", 1883, "admin", "123456");
		sub.subscriber(isp.first, MqttDemo);
		TRpcClient.client("localhost", 1884, 600, new TRpcClient() {
			override <T> call(TProtocol protocol) {
				var client = ServiceLoader.load(Gateway.Iface, protocol); // Gateway.Client.cast(result);
				var resp = client.get("Client").call(ISpi.name, "cat-print", null);
				println(resp.toString());
			}
		});
	}
}
