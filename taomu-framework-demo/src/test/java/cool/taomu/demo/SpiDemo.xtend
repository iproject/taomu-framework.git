package cool.taomu.demo

import cool.taomu.framework.service.rpc.Gateway
import cool.taomu.framework.service.rpc.TRpcClient
import cool.taomu.framework.utils.spi.ServiceLoader
import org.apache.thrift.protocol.TProtocol
import cool.taomu.framework.utils.spi.Alias

@FunctionalInterface
interface ISpi {
	def String print(String a);
}

@Alias("cat")
class Cat implements ISpi {

	override print(String a) {
		println("Hello Cat")
		return "Cat";
	}
}

@Alias("dog")
class Dog implements ISpi {

	override print(String a) {
		println("Hello Dog")
		return "Dog";
	}

}

class SpiDemo {
	
	def static void main(String[] args) {
		TRpcClient.client("localhost", 1884, 600, new TRpcClient() {
			override <T> call(TProtocol protocol) {
				var client = ServiceLoader.load(Gateway.Iface, protocol); // Gateway.Client.cast(result);
				var resp = client.get("Client").call(ISpi.name, "cat", null);
				println(resp.toString());
			}
		});
	// var s = ServiceLoader.load(ISpi);
	// s.get.forEach[it.print();];
	}
}
