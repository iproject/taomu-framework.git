package cool.taomu.demo

import joinery.DataFrame

class JoineryDfDemo {
	def static void main(String[] args) {
		var df = DataFrame.readCsv("./usecase/a.csv");
		var index = df.size
		for (var i = 0; i < index; i++) {
			println(df.get(i,"a"))
			println(df.get(i,"b"))
		}
	}
}
