package cool.taomu.demo

import cool.taomu.framework.utils.reflect.CallSiteUtils

class Operation {
	def Integer operate(Integer a, Integer b, Integer c) {
		a + b - c
	}

	def String operate(String a, String c) {
		return a + c
	}
}

@FunctionalInterface
interface Operator {
//	def Integer operate(Integer a, Integer b, Integer c);
	def String operate(String a, String b);
}

class CallSiteDemo {
	def static void main(String[] args) {
		// TODO interface interMethod implName 
		var a = new CallSiteUtils(Operator);
		var b = a.bind(Operation, "Hello", "aaa!!");
		// var c = b.invoke("operate", "Hello"," World!!");
		// var c = b.invoke("toOperate", "operate", 1,2,5);
		println(b);
	// println(c.toOperate(1, 2, 5));
	}
}
