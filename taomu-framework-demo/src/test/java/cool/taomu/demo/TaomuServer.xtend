package cool.taomu.demo

import cool.taomu.framework.service.mqtt.broker.MQTTBroker
import cool.taomu.framework.service.utils.CommonUtils

class TaomuServer {
	def static void main(String[] args) {
		CommonUtils.exec([
			var mqtt = new MQTTBroker;
			mqtt.startTcpServer();
			
		]);

	}
}
