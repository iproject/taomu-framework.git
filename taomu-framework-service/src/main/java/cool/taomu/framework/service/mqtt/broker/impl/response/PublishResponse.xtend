/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.service.mqtt.broker.impl.response

import cool.taomu.framework.service.mqtt.broker.entity.PublishEntity
import cool.taomu.framework.service.mqtt.broker.inter.IResponse
import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled
import io.netty.handler.codec.mqtt.MqttFixedHeader
import io.netty.handler.codec.mqtt.MqttMessageType
import io.netty.handler.codec.mqtt.MqttPublishMessage
import io.netty.handler.codec.mqtt.MqttPublishVariableHeader

class PublishResponse implements IResponse<PublishEntity> {

	override response(PublishEntity entity) {
		var header = new MqttFixedHeader(MqttMessageType.PUBLISH, entity.dup, entity.qos, false, 0);
		var varHeader = new MqttPublishVariableHeader(entity.topicName, entity.messageId);
		var ByteBuf heapBuf;
		if (entity.payload === null) {
			heapBuf = Unpooled.EMPTY_BUFFER;
		} else {
			try {
				heapBuf = Unpooled.wrappedBuffer(entity.payload);
			}catch(IllegalArgumentException e){
				e.printStackTrace;
			}
		}
		return new MqttPublishMessage(header, varHeader, heapBuf);
	}

}
