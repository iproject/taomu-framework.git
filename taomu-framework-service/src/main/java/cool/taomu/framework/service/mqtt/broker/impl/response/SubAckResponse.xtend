/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.service.mqtt.broker.impl.response

import cool.taomu.framework.service.mqtt.broker.entity.SubAckEntity
import cool.taomu.framework.service.mqtt.broker.entity.TopicEntity
import cool.taomu.framework.service.mqtt.broker.inter.IResponse
import io.netty.handler.codec.mqtt.MqttFixedHeader
import io.netty.handler.codec.mqtt.MqttMessageIdVariableHeader
import io.netty.handler.codec.mqtt.MqttMessageType
import io.netty.handler.codec.mqtt.MqttQoS
import io.netty.handler.codec.mqtt.MqttSubAckMessage
import io.netty.handler.codec.mqtt.MqttSubAckPayload
import java.util.ArrayList
import java.util.List

class SubAckResponse implements IResponse<SubAckEntity> {

	override response(SubAckEntity entity) {
		var header = new MqttFixedHeader(MqttMessageType.SUBACK, false, MqttQoS.AT_MOST_ONCE, false, 0);
		var varHeader = MqttMessageIdVariableHeader.from(entity.messageId);
		return new MqttSubAckMessage(header, varHeader, new MqttSubAckPayload(getTopicQos(entity.qos)));
	}

	def List<Integer> getTopicQos(List<TopicEntity> topics) {
		var qoss = new ArrayList<Integer>(topics.size());
		for (TopicEntity topic : topics) {
			qoss.add(topic.getQos());
		}
		return qoss;
	}

}
