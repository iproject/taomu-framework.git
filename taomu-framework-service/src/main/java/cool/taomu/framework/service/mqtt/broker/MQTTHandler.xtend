/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.service.mqtt.broker

import cool.taomu.framework.service.mqtt.broker.inter.IRequest
import cool.taomu.framework.service.utils.CommonUtils
import cool.taomu.framework.utils.spi.ServiceLoader
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInboundHandlerAdapter
import io.netty.handler.codec.mqtt.MqttMessage
import org.slf4j.LoggerFactory
import io.netty.channel.ChannelHandler.Sharable
import static org.junit.Assert.assertNotNull

@Sharable
class MQTTHandler extends ChannelInboundHandlerAdapter {
	val LOG = LoggerFactory.getLogger(MQTTHandler);

	override channelRead(ChannelHandlerContext ctx, Object obj) {
		val mqttMessage = obj as MqttMessage;
		if (mqttMessage !== null && mqttMessage.decoderResult().isSuccess()) {
			var messageType = mqttMessage.fixedHeader().messageType();
			val typeName = messageType.name;
			LOG.debug("收到请求类型：{}", typeName);
			val msg = ServiceLoader.load(IRequest).get(typeName);
			// val msg = mqttConfig.mqttTypes.get(messageType.name);
			CommonUtils.exec([
				val msgs = msg.request(ctx, mqttMessage);
				if (msgs !== null) {
					msgs.filterNull.forEach [ r |
						assertNotNull(r);
						ctx.writeAndFlush(r)
					];
				}
			])
		} else {
			ctx.close();
		}
	}
}
