/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.service.mqtt.broker.impl

import com.google.common.collect.ArrayListMultimap
import cool.taomu.framework.cache.KeyValueCache
import cool.taomu.framework.service.utils.CommonUtils
import cool.taomu.framework.service.mqtt.broker.entity.MessageEntity
import cool.taomu.framework.service.mqtt.broker.inter.IPublishObserver
import cool.taomu.framework.service.mqtt.broker.inter.IPublishObserver.Type
import java.util.Set
import java.util.Vector
import org.apache.commons.lang3.SerializationUtils
import org.slf4j.LoggerFactory

import static extension cool.taomu.framework.service.utils.CommonUtils.*

final class PublishObservable {
	var LOG = LoggerFactory.getLogger(PublishObservable);
	val static ArrayListMultimap<String, IPublishObserver> observers = ArrayListMultimap.create();
	val static instance = new PublishObservable();
	KeyValueCache cache = KeyValueCache.instance;

	private new() {
	}

	def synchronized static getInstance() {
		return instance;
	}

	def synchronized void register(String key, IPublishObserver arg) {
		LOG.info("register :" + key);
		observers.put(key, arg);
	}

	def synchronized void unregister(String key) {
		LOG.info("unregister :" + key);
		observers.removeAll(key);
		LOG.info("unregister :" + observers.size);
	}

	def void start(String id, Type type) {
		synchronized (observers) {
			LOG.info("observers number : {}", observers.size);
			val messages = new Vector<MessageEntity>();
			if (type.equals(Type.RETAIN)) {
				// 获取所有存储的ratein消息
				var rateins = cache.get("*".retain) as Set<MessageEntity>;
				if (rateins !== null) {
					LOG.info("rateins 消息数 : {}", rateins.size)
					messages.addAll(rateins);
				}
			}
			if (type.equals(Type.MESSAGE)) {
				messages.add(cache.get(id) as MessageEntity);
			}
			observers.forEach([ k, v |
				CommonUtils.exec([
					v.update(SerializationUtils.clone(messages), type)
				])
			])
		}
	}
}
