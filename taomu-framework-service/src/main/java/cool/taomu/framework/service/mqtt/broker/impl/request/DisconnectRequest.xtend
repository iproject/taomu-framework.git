/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.service.mqtt.broker.impl.request

import cool.taomu.framework.cache.KeyValueCache
import cool.taomu.framework.service.mqtt.broker.entity.ClientSessionEntity
import cool.taomu.framework.service.mqtt.broker.impl.PublishObservable
import cool.taomu.framework.service.mqtt.broker.inter.IRequest
import cool.taomu.framework.service.utils.CommonUtils
import cool.taomu.framework.utils.spi.Alias
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.mqtt.MqttMessage
import org.slf4j.LoggerFactory

import static extension cool.taomu.framework.service.utils.CommonUtils.*

/**
 *  DISCONNECT 	|	14 	|	两个方向都允许	|	断开连接通知
 */
@Alias(value="DISCONNECT")
class DisconnectRequest implements IRequest {
	val LOG = LoggerFactory.getLogger(DisconnectRequest);

	KeyValueCache cache = KeyValueCache.instance;

	override request(ChannelHandlerContext ctx, MqttMessage mqttMessage) {
		LOG.info("执行了MQTT Disconnect 命令")
		var clientId = CommonUtils.getClientId(ctx.channel);
		// 1. 清理会话 或 重新设置该客户端会话状态
		var clientSession = cache.get(clientId.session) as ClientSessionEntity;
		if (clientSession.cleanStatus) {
			cache.remove(clientId.session);
		}
		PublishObservable.instance.unregister(clientId);
		ctx.close;
		return null;
	}

}
