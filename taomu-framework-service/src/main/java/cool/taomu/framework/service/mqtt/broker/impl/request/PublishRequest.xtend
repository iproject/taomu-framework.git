/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.service.mqtt.broker.impl.request

import com.google.gson.Gson
import cool.taomu.framework.cache.KeyValueCache
import cool.taomu.framework.configure.ConfigureManage
import cool.taomu.framework.service.mqtt.broker.entity.MessageEntity
import cool.taomu.framework.service.mqtt.broker.entity.PubAckEntity
import cool.taomu.framework.service.mqtt.broker.impl.PublishObservable
import cool.taomu.framework.service.mqtt.broker.impl.response.PubAckResponse
import cool.taomu.framework.service.mqtt.broker.inter.IPublishObserver.Type
import cool.taomu.framework.service.mqtt.broker.inter.IRequest
import cool.taomu.framework.service.rpc.Gateway
import cool.taomu.framework.service.rpc.TRpcClient
import cool.taomu.framework.service.utils.CommonUtils
import cool.taomu.framework.utils.spi.Alias
import cool.taomu.framework.utils.spi.ServiceLoader
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.mqtt.MqttMessage
import io.netty.handler.codec.mqtt.MqttPublishMessage
import io.netty.handler.codec.mqtt.MqttQoS
import io.netty.util.ReferenceCountUtil
import org.apache.thrift.protocol.TProtocol
import org.slf4j.LoggerFactory

import static extension cool.taomu.framework.service.utils.CommonUtils.*

/**
 *  PUBLISH 	|	3 	|	两个方向都允许 	|	发布消息
 */
@Alias(value="PUBLISH")
class PublishRequest implements IRequest {
	val LOG = LoggerFactory.getLogger(PublishRequest)

	KeyValueCache cache = KeyValueCache.instance;

	val config = ConfigureManage.loadConfig;

	override request(ChannelHandlerContext ctx, MqttMessage mqttMessage) {
		try {
			var publishMessage = mqttMessage as MqttPublishMessage;
			var message = new MessageEntity()
			message.senderId = CommonUtils.getClientId(ctx.channel())
			LOG.info("执行了MQTT Publish 命令 : " + message.senderId);
			message.qos = publishMessage.fixedHeader().qosLevel();
			message.topic = publishMessage.variableHeader().topicName();
			message.payload = (mqttMessage as MqttPublishMessage).payload();
			message.type = mqttMessage.fixedHeader().messageType().value();
			message.dup = publishMessage.fixedHeader().isDup();
			message.retain = publishMessage.fixedHeader().isRetain();
			message.msgId = publishMessage.variableHeader().packetId();
			message.senderChannel = ctx.channel;
			switch (message.qos) {
				case EXACTLY_ONCE,
				case AT_MOST_ONCE: {
					LOG.info(String.format("Qos0 message,clientId=%s", message.senderId));
					retainMessage(message);
				}
				case AT_LEAST_ONCE: {
					LOG.info(String.format("Qos1 message,clientId=%s", message.senderId));
					retainMessage(message);
					var messageId = message.msgId;
					var entity = new PubAckEntity(messageId);
					// 回给发送者
					return #[new PubAckResponse().response(entity)];
				}
				default: {
					LOG.info(String.format("Wrong mqtt message,clientId=%s", message.senderId));
				}
			}
		} catch (Exception ex) {
			LOG.debug("执行了MQTT Publish 命令出错了 : ", ex);
		} finally {
			ReferenceCountUtil.release(mqttMessage.payload());
		}
		return null;
	}

	protected synchronized def retainMessage(MessageEntity message) {
		LOG.debug(
			"clientId 为 {} 是否存在 Retain 数据 {}, 接受到的数据为 {} ",
			message.senderId,
			message.retain,
			new String(message.payload)
		);
		// 存储消息
		cache.store(message.senderId, message);
		// 需要功能验证
		if (message.retain) {
			var qos = message.qos;
			var payload = message.payload;
			// msgVal.clientId 为发送者的clientId
			if (qos === MqttQoS.AT_MOST_ONCE || payload === null || payload.length === 0) {
				LOG.info("清空 clientId 为 {} 的Retain数据", message.senderId);
				cache.remove(message.senderId.retain);
			} else {
				LOG.info("保存 clientId 为 {} 的Retain数据", message.senderId);
				cache.store(message.senderId.retain, message);
				CommonUtils.exec([
					config.mqtt.cluster.filterNull.forEach [ addr |
						CommonUtils.exec([
							TRpcClient.client(!addr.hostname.equals("") ? addr.hostname : "localhost", addr.port, 600,
								new TRpcClient() {
									override <T> call(TProtocol protocol) {
										var client = ServiceLoader.load(Gateway.Iface, protocol); // Gateway.Client.cast(result);
										var resp = client.get("Client").refresh(addr.hostname);
										LOG.info(resp.toString());
									}
								});
						]);
					]
				]);
			}
		}
		// 推送用户订阅
		PublishObservable.instance.start(message.senderId, Type.MESSAGE);
		CommonUtils.exec([
			config.mqtt.cluster.filterNull.forEach [ addr |
				CommonUtils.exec([
					val config = ConfigureManage.loadConfig
					config.mqtt.cluster.forEach [ cluster |
						TRpcClient.client(!cluster.hostname.equals("") ? cluster.hostname : "localhost", cluster.port,
							600, new TRpcClient() {
								override <T> call(TProtocol protocol) {
									var client = ServiceLoader.load(Gateway.Iface, protocol); // Gateway.Client.cast(result);
									var resp = client.get("Client").publish(config.mqtt.hostname,
										new Gson().toJson(message));
									LOG.info(resp.toString());
								}
							});
					]
				]);
			]
		]);
	}
}
