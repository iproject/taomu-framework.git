package cool.taomu.framework.service.rpc

import java.lang.invoke.CallSite
import java.lang.invoke.ConstantCallSite
import java.lang.invoke.LambdaMetafactory
import java.lang.invoke.MethodHandle
import java.lang.invoke.MethodHandles
import java.lang.invoke.MethodType
import org.slf4j.LoggerFactory

class ServiceCallSite {

	static val LOG = LoggerFactory.getLogger(ServiceCallSite);

	def static service() {
	}

	def static CallSite bootstrap(MethodHandles.Lookup caller, String name,
		MethodType type) throws NoSuchMethodException, IllegalAccessException {
		var MethodHandles.Lookup lookup = MethodHandles.lookup();
		var Class<?> thisClass = lookup.lookupClass();
		var MethodHandle sayHello = lookup.findStatic(thisClass, "clientCallSite",
			MethodType.methodType(Object, String, String, String));
		// LambdaMetafactory.
		return new ConstantCallSite(sayHello);
	}

}
