/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.service.mqtt.broker

import cool.taomu.framework.configure.entity.ConfigureEntity
import io.netty.channel.socket.SocketChannel
import io.netty.handler.ssl.ClientAuth
import io.netty.handler.ssl.SslContextBuilder
import io.netty.handler.ssl.SslHandler
import io.netty.handler.ssl.SslProvider
import cool.taomu.framework.crypto.ssl.SslUtil

class MQTTSslHandler {
    def static build(SocketChannel channel,ConfigureEntity config) {
        var sslConfig = config.mqtt.ssl;
        var ssl = new SslUtil(sslConfig.keyStoreType);
        var kmf = ssl.keyManageFactory(sslConfig.keyFilePath, sslConfig.managerPwd, sslConfig.storePwd);
        var contextBuilder = SslContextBuilder.forServer(kmf);
        var sslContext = contextBuilder.sslProvider(SslProvider.valueOf("JDK")).build();
        var sslEngine = sslContext.newEngine(channel.alloc, channel.remoteAddress.hostString,
            channel.remoteAddress.port);
        sslEngine.useClientMode = false;
        if (sslConfig.useClientCA) {
            contextBuilder.clientAuth(ClientAuth.REQUIRE);
            contextBuilder.trustManager(ssl.trustManageFactory);
            sslEngine.needClientAuth = true;
        }
       return new SslHandler(sslEngine);
    }
}
