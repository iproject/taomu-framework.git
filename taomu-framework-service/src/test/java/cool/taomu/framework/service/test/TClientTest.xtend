package cool.taomu.framework.service.test

import cool.taomu.framework.service.rpc.Gateway
import cool.taomu.framework.service.rpc.TRpcClient
import cool.taomu.framework.utils.spi.ServiceLoader
import org.apache.thrift.protocol.TProtocol

class TClientTest {
	def static void main(String[] args) {
		TRpcClient.client("localhost", 8899, 600, new TRpcClient() {

			override <T> call(TProtocol protocol) {
				var client = ServiceLoader.load(Gateway.Iface); // Gateway.Client.cast(result);
				//var resp = client.first.call("aa", "aa");
				//println(resp);
			}
		})
	}
}
