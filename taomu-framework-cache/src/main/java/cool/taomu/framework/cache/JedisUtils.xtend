/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.cache

import cool.taomu.framework.configure.ConfigureManage
import java.util.HashSet
import org.apache.commons.pool2.impl.GenericObjectPoolConfig
import org.slf4j.LoggerFactory
import redis.clients.jedis.Connection
import redis.clients.jedis.HostAndPort
import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisCluster
import redis.clients.jedis.JedisPool
import redis.clients.jedis.commands.JedisCommands

/**
 * 名词 加ed 变 形容词
 * 动词 加ed 变 过去式
 * 动词 加ing 变 正在进行时
 */
class JedisUtils implements AutoCloseable {
	val static LOG = LoggerFactory.getLogger(JedisUtils)
	var JedisCluster cluster = null;
	var JedisPool pool = null;
	var config = ConfigureManage.loadConfig

	val static instance = new JedisUtils()

	private new() {
		if (config.cache.jedis !== null)
			if (config.cache.jedis.isClustered) {
				initJedisCluster
			} else {
				initJedisPool
			}
	}

	def static synchronized getInstance() {
		return instance
	}

	private def initJedisPool() {
		var config = ConfigureManage.loadConfig
		if (config.cache.jedis !== null) {
			var gopc = new GenericObjectPoolConfig<Jedis>();
			gopc.maxIdle = config.cache.jedis.maxIdle;
			gopc.minIdle = config.cache.jedis.minIdle;
			gopc.maxTotal = config.cache.jedis.maxTotal;
			gopc.testOnBorrow = config.cache.jedis.testOnBorrow;
			gopc.testOnReturn = config.cache.jedis.testOnReturn;
			var password = config.cache.jedis.password;
			var user = config.cache.jedis.username;
			val hostAndPort = new HashSet<HostAndPort>();
			var timeout = config.cache.jedis.timeout;
			config.cache.jedis.getCluster.forEach [ host |
				LOG.info("创建Redis连接: {}:{}", host.ip, host.port);
				hostAndPort.add(new HostAndPort(host.ip, host.port));
			]
			if (user === null) {
				if (password === null) {
					pool = new JedisPool(gopc, hostAndPort.get(0).host, hostAndPort.get(0).port);
				} else {
					pool = new JedisPool(gopc, hostAndPort.get(0).host, hostAndPort.get(0).port, timeout, password);
				}
			} else if (user !== null && password !== null) {
				pool = new JedisPool(gopc, hostAndPort.get(0).host, hostAndPort.get(0).port, timeout, user, password);
			}
		}
	}

	private def initJedisCluster() {
		var config = ConfigureManage.loadConfig
		if (config.cache.jedis !== null) {
			try {
				var gopc = new GenericObjectPoolConfig<Connection>();
				gopc.maxIdle = config.cache.jedis.maxIdle;
				gopc.minIdle = config.cache.jedis.minIdle;
				gopc.maxTotal = config.cache.jedis.maxTotal;
				gopc.testOnBorrow = config.cache.jedis.testOnBorrow;
				gopc.testOnReturn = config.cache.jedis.testOnReturn;
				val hostAndPort = new HashSet<HostAndPort>();
				config.cache.jedis.getCluster.forEach [ host |
					LOG.info("创建Redis连接: {}:{}", host.ip, host.port);
					hostAndPort.add(new HostAndPort(host.ip, host.port));
				]
				var timeout = config.cache.jedis.timeout;
				var maxAttempts = config.cache.jedis.maxAttempts;
				var password = config.cache.jedis.password;
				var user = config.cache.jedis.username;
				if (user === null) {
					if (password === null) {
						cluster = new JedisCluster(hostAndPort, timeout, timeout, maxAttempts, gopc);
					} else {
						cluster = new JedisCluster(hostAndPort, timeout, timeout, maxAttempts, password, gopc);
					}
				} else if (user !== null && password !== null) {
					cluster = new JedisCluster(hostAndPort, timeout, timeout, maxAttempts, user, password, null, gopc);
				}
				Runtime.runtime.addShutdownHook(new Thread() {
					override run() {
						if (cluster !== null) {
							LOG.info("addShutdownHook 关闭了 Redis");
							cluster.close
						}
					}
				})
			} catch (Exception e) {
				if (cluster !== null) {
					LOG.info("关闭了 Redis");
					this.cluster.close
				}
				if (this.pool !== null) {
					LOG.info("关闭了 Redis");
					this.pool.close
				}
				LOG.error("创建Redis失败", e);
				this.cluster = null;
				this.pool = null;
			}
		}
	}

	def JedisCommands getJedis() {
		if (this.cluster !== null) {
			return this.cluster;
		} else if (this.pool !== null) {
			return this.pool.borrowObject;
		}
		return null;
	}

	override close() throws Exception {
		if (this.pool !== null) {
			this.pool.close
		}
	}

}
