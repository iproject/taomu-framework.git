package cool.taomu.framework.client.mqtt

import cool.taomu.framework.crypto.Base64
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import org.slf4j.LoggerFactory

class Sender {
	val LOG = LoggerFactory.getLogger(Sender);

	def send(SenderEntity sender) {
		this.send(sender, false);
	}

	def static sendOut(SenderEntity sender) {
		new Sender().send(sender);
	}

	def static sendOut(SenderEntity sender, boolean isSsl) {
		new Sender().send(sender, isSsl);
	}

	def send(SenderEntity sender, boolean isSsl) {
		LOG.info("mqtt sender info:" + sender.toString());
		try (var memoryPersistence = new MemoryPersistence()) {
			var mqttConnectOptions = new MqttConnectOptions();
			mqttConnectOptions.setCleanSession(true);
			mqttConnectOptions.setUserName(sender.username);
			mqttConnectOptions.setPassword(sender.password.toCharArray());
			mqttConnectOptions.setConnectionTimeout(sender.timeout);
			mqttConnectOptions.setKeepAliveInterval(20);
			var mqttClient = new MqttClient(String.format("%s://%s:%d", isSsl ? "ssl" : "tcp", sender.ip, sender.port),
				sender.clientId, memoryPersistence);

			mqttClient.connect(mqttConnectOptions);
			var mqttMessage = new MqttMessage(new Base64(sender.message.bytes).encode());
			mqttMessage.setQos(sender.qos.value);
			mqttMessage.setRetained(sender.retain);
			mqttClient.publish(sender.topic, mqttMessage);
			LOG.debug("clientId: {} 发送成功", sender.clientId);
			mqttClient.disconnect; // 这个位置可以要修改
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
}
